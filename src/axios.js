import axios from 'axios'

export const baseUrl = 'http://localhost/cementerio/public' 
// export const baseUrl = 'http://localhost:8181/cementerio/public'


export const HTTP = axios.create({
    baseURL: baseUrl,
    headers: {
        Authorization: 'Bearer {token}'
    }
})