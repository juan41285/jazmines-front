import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import VueMoment from 'vue-moment'
import VueHtmlToPaper from 'vue-html-to-paper';
import Vue2Filters from 'vue2-filters'

Vue.use(Vue2Filters)

const moment = require('moment')
require('moment/locale/es')
const options = {
  name: '_blank',
  specs: [
    'fullscreen=no',
    'titlebar=no',
    'scrollbars=no'
  ]
}

Vue.use(VueHtmlToPaper, options);
Vue.use(VueMoment, {
  moment
})
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
  
}).$mount('#app')
