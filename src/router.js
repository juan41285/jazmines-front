import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import addCliente from './views/addCliente.vue'
import editCliente from './views/editCliente.vue'
import Cliente from './views/Cliente.vue'
import Parcelas from './views/Parcelas.vue'
import Parcela from './views/Parcela.vue'
import Plano from './views/Plano.vue'
import Cuadro from './views/Cuadro.vue'
import Informes from './views/Informes.vue'
import cobradores from './views/cobradores.vue'
import comprobante from './components/comprobante.vue'

// import ListadoCliente from './components/ListadoCliente.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/detalle-parcela/:parcela',
      name: 'detalleParcela',
      component: Parcela
    },
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/nuevo-cliente',
      name: 'cliente',
      component: addCliente
    },
    {
      path: '/editar-cliente/:id',
      name: 'editar',
      component: editCliente
    },
    { 
      path: '/cliente/:id/:tab',
      name: 'cliente-detalle',
      component: Cliente
    },
    {
      path: '/parcelas',
      name: 'parcelas',
      component: Parcelas
    },
    {
      path: '/plano',
      name: 'plano',
      component: Plano
    },
    {
      path: '/plano/detalle/:manzana/:cuadro',
      name: 'plano',
      component: Cuadro
    },
    {
      path: '/comprobante',
      name: 'comprobante',
      component: comprobante
    },
    {
      path: '/informes',
      name: 'informes',
      component: Informes
    },
    {
      path: '/cobradores',
      name: 'cobradores',
      component: cobradores
    },
    
    
  ]
})
